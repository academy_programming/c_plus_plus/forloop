#include <iostream>
using namespace std;

int main() {

    setlocale(0, LC_ALL);
    /* Цикл for
     * 
     * Общий синтаксис
     * for(переменная_счетчик; условие выхода; изменение_переменной_счетчика)
     * {
     *      // тело цикла;
     * }
     * 
     * */

    /*int counter = 0;

    for (counter = 0; counter < 10; counter++) {
        cout << counter;
    }*/

    // ----------------------------------------------------

    /* Задача
     *
     * Используя цикл FOR
     * напишите программу, которая дает пользователю возможность вводить числа
     * с клавиатуры, до тех пор , пока не будет введен 0. При вводе нуля,
     * необходимо выводить сумму всех введенных до этого чисел.
     * */

    /*
     * Цикл до изучения оператора break;
     *

    int summ = 0;
    int number;
    for (;;) {
        cin >> number;
        summ += number;
        if (number == 0) {
            cout << summ << endl;
        }
    } */

    // ----------------------------------------------------

    /*int summ = 0;
    int number;
    for (;;) {
        cin >> number;
        summ += number;
        if (number == 0) {
            cout << summ << endl;
            continue;
        }
        cout << "not continue!" << endl;
    }*/

    // ----------------------------------------------------

    /* Задача (continue)
     *
     * Пользователь вводит число с клавиатуры
     * Вывести все числа кратные 2м до этого числа включительно
     *
     * */

    /*int number;
    cin >> number;
    for (int i = 0; i < number; ++i) {
        if (i % 2 != 0)
            continue;
        cout << i << endl;
    }*/

    // ----------------------------------------------------

    // Вложенные циклы


    int w, h;

    do {
        cout << "Enter sizes: ";
        cin >> w >> h;
        for (int i = 0; i < h; ++i) {
            for (int j = 0; j < w; ++j) {
                cout << "*";
            }
            cout << endl;
        }
    } while (w != 0 || h != 0);
}
